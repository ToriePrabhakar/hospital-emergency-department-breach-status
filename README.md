# Hospital Emergency Department Breach Status Reporter

## Introduction

The [Clinical Developers Club](https://clinicaldevelopers.org) released this
programming [challenge](https://github.com/Clinical-Developers/Clinical_Developer_Challenges/tree/master/Breach/README.md), addressing patient wait times in the emergency department
of fictious hosptials, on 05 September 2017.

## The Challenge

In this fight for survival you are given the unenviable task of assessing the
performance of hospital emergency departments across the country. A list of
hospitals to be inspected is required so patient wait times in the subpar
emergency deparments can be decreased.

As a highly-skilled and sought after clinical programming professional you
steadfastly refuse to work anywhere other than your home. A daily data feed
will be electronically transferred from central healthcare control to your
computing environent. The data feed is comprised of a series of array-like
structures with each element separated by a comma. The data feed might or might
not include pair-matched brackets ([ and ]) and these might be nested
arbitrarily deep. No matter, as these brackets are superflucous to the task at
hand. Each array-like structure contains the statistics on the performance of
each emergency department each day of the week. A breach is defined as a
patient waiting more than 12 hours to be assessed.

- If a hospital has no breaches at all it is to be labelled as 'Safe'.
- If a hospital has 1 to 3 breaches it is to be labelled as 'Trouble'.
- If a hospital has over 3 breaches it is to be labelled as 'Urgent Inspection'.

## Examples

If the input is ['safe','safe','safe','safe','safe','safe','safe'], then the
status should be 'Safe'.

If the input is ['safe','breach','safe','safe','breach'], then the status
should be 'Trouble'.

If the input is ['safe','breach','safe','safe','breach','safe','breach',
'breach'], then the status should be 'Urgent Inspection'.

## Implementation

This solution is implemented using the *nix awk utility and programming
language, and again using the Perl programming language. While development and
testing was performed using the ash command processing shell, the awk script
should execute in most compliant *nix shells with little to no modification.
The Perl script requires no modification.

Our website https://www.themarioninstitute.org/ and healthcare institute both are using this app as it is very helpful for emergency department. With the help of this, we can monitor the whole process and sessions between the clients and the psychologists.   
